# Post Getter
Post Getter is a simple wrapper around Twitter API for obtaining and saving posts from twitter together with attachments in sqlite database.

Architecture of the library allows simple to extend it for supporting work with other social networks and to add new formats for data saving.
- - - -
# List of requared python libraries:
* [requests](http://docs.python-requests.org/en/master/) - HTTP for Humans =)
* [twitter](https://pypi.python.org/pypi/twitter/) - API for twitter.com

These libraries can be installed with next commands from command prompt:
```sh
pip install requests
pip install twitter
```
**For developing was used Python 3.5.2**
- - - -
## Example of usage: ##
```sh
import pg.facade as pgf
pgf.save_tweets('barackobama')
```
The code above saves tweets of user *barackobama* in the *barackobama/barackobama.db* and downloads attachments in the *barackobama/attachments* folder. The more parametrized method is as follows:
```sh
pgf.save_tweets('barackobama', load_attachments=False, 'path/to/save')
```
The next example shows creating and using main classes: TwitterReceiver, SqliteSaver.
```sh
import pg.receivers.factory as rfact
import pg.savers.factory as sfact

tr = rfact.tweets_receiver("barackobama")
ss = sfact.sqlite_saver("name_of_db_with_posts", "path/to/save/db")

while tr.has_more():
    ss.save_all(tr.get_posts())
```
Also TwitterReceiver supports iterating over the elements: ([how it works in python](https://docs.python.org/3.1/tutorial/classes.html#iterators))
```sh
import pg.receivers.factory as rfact

tr = rfact.tweets_receiver("barackobama")
for t in tr:
    print(t.text())
```
Then we can download all mp4 files for example such:
```sh
import pg.receivers.factory as rfact
import pg.loader as ldr

tr = rfact.tweets_receiver('barackobama')
mp4 = [obj for t in tr for obj in t.attachments() if obj.url().endswith('.mp4')]
ldr.load_all('obama_mp4/', mp4)
```
And for access to specific fields of a post you can use raw_data() function.
The next example prints count of all retweets for "barackobama" user.
```sh
import pg.receivers.factory as rfact

tr = rfact.tweets_receiver("barackobama")
print(sum([t.raw_data()['retweet_count'] for t in tr]))
```
- - - -
# License
Free Software, Hell Yeah!

