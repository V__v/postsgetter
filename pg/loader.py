import multiprocessing
import requests
import os


def load(path, attachment):
    ext = os.path.splitext(attachment.url())[1]
    with open(path + attachment.id() + ext, 'wb') as f:
        f.write(requests.get(attachment.url()).content)


def load_all(path, attachments):
    pool = multiprocessing.Pool()
    pool.starmap(load, [(path, obj) for obj in attachments])


def load_attachments(path, post):
    if not os.path.exists(path):
        os.makedirs(path)
        path += '/'
    load_all(path, post.attachments())


def load_all_attachments(path, posts):
    if not os.path.exists(path):
        os.makedirs(path)
    path += '/'
    attachments = [obj for post in posts for obj in post.attachments()]
    load_all(path, attachments)