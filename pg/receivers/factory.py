from pg.receivers.twitter import twitter


def tweets_receiver(nick_name, observer=None):
    return twitter.TwitterReceiver(nick_name, observer)


def vk_receiver():
    raise NotImplementedError()
