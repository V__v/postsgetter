class IPost:
    def __init__(self):
        raise NotImplementedError()

    def id(self):
        raise NotImplementedError()

    def text(self):
        raise NotImplementedError()

    def date(self):
        raise NotImplementedError()

    def likes(self):
        raise NotImplementedError()

    def attachments(self):
        raise NotImplementedError()

    def raw_data(self):
        raise NotImplementedError()
