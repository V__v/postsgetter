from pg.receivers.interfaces.iattachment import IAttachment


class TwitterAttachment(IAttachment):
    def __init__(self, obj):
        self.obj = obj

    def id(self):
        try:
            return self.obj['id_str']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""

    def url(self):
        try:
            if 'video_info' in self.obj:
                return self.obj['video_info']['variants'][0]['url']
            else:
                return self.obj['media_url']
        except KeyError as e:
            print("Wrong key " + str(e))
            return ""
