from pg.savers.sqlite import sqlite


def sqlite_saver(db_name, path):
    return sqlite.SqliteSaver(db_name, path)


def csv_saver():
    raise NotImplementedError()
