class ISaver:
    def __init__(self):
        raise NotImplementedError()

    def save_all(self, posts):
        raise NotImplementedError()

    def save(self, post):
        raise NotImplementedError()
