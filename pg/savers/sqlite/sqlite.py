from pg.savers.interfaces.isaver import ISaver
import sqlite3
import codecs
import os


class SqliteSaver(ISaver):
    def __init__(self, db_name, path="."):
        self.db_name = db_name
        self.path = path
        self.path_to_db = "{0}/{1}.db".format(self.path, self.db_name)
        try:
            if not os.path.exists(self.path):
                os.makedirs(self.path)
            self.connection = sqlite3.connect(self.path_to_db)
            self.cursor = self.connection.cursor()
            self.cursor.execute('DROP TABLE IF EXISTS posts')
            self.cursor.execute('CREATE TABLE posts (id text, text text, date text, likes integer)')
            self.cursor.execute('DROP TABLE IF EXISTS attachments')
            self.cursor.execute('CREATE TABLE attachments (post_id text, id text, url text)')
        except (sqlite3.OperationalError, OSError) as e:
            print("Wrong parameters were be passed."
                  "\nError message: " + str(e))

    def close(self):
        self.connection.close()

    def save_all(self, posts):
        try:
            posts_to_insert = [(post.id(), post.text(), post.date(), post.likes()) for post in posts]
            self.cursor.executemany("INSERT INTO posts VALUES (?, ?, ?, ?)", posts_to_insert)
            attachments_to_insert = [(post.id(), obj.id(), obj.url()) for post in posts for obj in post.attachments()]
            self.cursor.executemany("INSERT INTO attachments VALUES (?, ?, ?)", attachments_to_insert)
            self.connection.commit()
        except (AttributeError, TypeError) as e:
            print("Wrong parameters were be passed."
                  "\nParameter posts must be a list of objects that are implementing IPost interface."
                  "\nError message: " + str(e))

    def save(self, post):
        try:
            record = (post.id(), codecs.encode(post.text()), post.date(), post.likes())
            self.cursor.execute("INSERT INTO posts VALUES (?, ?, ?, ?)", record)
            attachments_to_insert = [(obj.id(), obj.url()) for obj in post.attachments()]
            self.cursor.executemany("INSERT INTO attachments VALUES (?, ?)", attachments_to_insert)
            self.connection.commit()
        except (AttributeError, TypeError) as e:
            print("Wrong parameters were be passed."
                  "\nParameter post must be an object that is implementing IPost interface."
                  "\nError message: " + str(e))
