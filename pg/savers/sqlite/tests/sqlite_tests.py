from pg.savers.sqlite import sqlite
import unittest
import os


class SqliteSaverTests(unittest.TestCase):
    def setUp(self):
        self.ss = sqlite.SqliteSaver("test_db", ".")

    def test_init_wrong_parameters(self):
        try:
            sqlite.SqliteSaver(TypeError, ".")
            sqlite.SqliteSaver("db_name", "")
            sqlite.SqliteSaver("db_name", "WrongDisk:\\")
        except Exception as e:
            self.fail("__init__() raised exception unexpectedly!\nError: " + str(e))

    def test_save_wrong_parameters(self):
        try:
            self.ss.save(None)
        except Exception as e:
            self.fail("save() raised exception unexpectedly!\nError: " + str(e))
        finally:
            self.ss.close()
            os.remove(self.ss.path_to_db)

    def test_save_all_wrong_parameters(self):
        try:
            self.ss.save_all([])
        except Exception as e:
            self.fail("save_all() raised exception unexpectedly!\nError: " + str(e))
        finally:
            self.ss.close()
            os.remove(self.ss.path_to_db)
