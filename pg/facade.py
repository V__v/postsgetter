from pg.receivers import factory as rfact
from pg.savers import factory as sfact
from pg.observer import Observer
import pg.loader as ldr


def save_tweets(nick_name, is_load_attachments=True, path=".", observer=None):
    local_observer = Observer() if observer else None
    ss = sfact.sqlite_saver(nick_name, path)
    tr = rfact.tweets_receiver(nick_name, local_observer)
    observer.set_posts_count(local_observer.posts_count())
    path += "/attachments"
    while tr.has_more():
        posts = tr.get_posts()
        ss.save_all(posts)
        if is_load_attachments:
            ldr.load_all_attachments(path, posts)
        observer.set(local_observer.processed_count())
    observer.finish()


def save_vk_posts():
    raise NotImplementedError()
